import pymel.core as pm

nodes = [
    "OpenVDBRead",
    "OpenVDBTransform",
    "OpenVDBCopy",
    "OpenVDBFilter",
    "OpenVDBFromMayaFluid",
    "OpenVDBFromPolygons",
    ]

prev_select = pm.ls(l=True, sl=True, type=nodes, tail=1)

pm.createNode("OpenVDBWrite", n="OpenVDBWrite1")
new = pm.ls(l=True, sl=True, type=u"OpenVDBWrite")[0]
pm.connectAttr(u"time1.outTime", new + u".inputTime")

if prev_select != []:
    pm.connectAttr(prev_select[0] + ".VdbOutput", new + u".VdbInput")
