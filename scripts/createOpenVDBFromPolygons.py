import pymel.core as pm

nodes = [
    "transform",
    "mesh",
    "OpenVDBToPolygons"
    ]

def isTransform(node, vdb):
    prim = pm.listRelatives(node, type=u"mesh")
    if prim != []:
        pm.connectAttr(prim[0] + u".outMesh", vdb + u".MeshInput")
    return 0

prev_select = pm.ls(l=True, sl=True, type=nodes, tail=1)

if prev_select != []:
    prev_type = pm.nodeType(prev_select[0])

pm.createNode("OpenVDBFromPolygons", n="OpenVDBFromPolygons1")
new = pm.ls(l=True, sl=True, type=u"OpenVDBFromPolygons")[0]

if prev_select == []:
    pass
elif prev_type == u"OpenVDBToPolygons":
    pm.connectAttr(prev_select[0] + u".meshOutput[0]", new + u".MeshInput")
elif prev_type == u"mesh":
    pm.connectAttr(prev_select[0] + u".outMesh", new + u".MeshInput")
else:
    isTransform(prev_select[0], new)
