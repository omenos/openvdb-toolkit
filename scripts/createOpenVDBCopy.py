import pymel.core as pm

nodes = [
    "OpenVDBRead",
    "OpenVDBTransform",
    "OpenVDBCopy",
    "OpenVDBFilter",
    "OpenVDBFromMayaFluid",
    "OpenVDBFromPolygons",
    "OpenVDBWrite"
    ]

prev_select = pm.ls(l=True, sl=True, type=nodes, tail=2)
prev_num = len(prev_select)

pm.createNode("OpenVDBCopy", n="OpenVDBCopy1")
new = pm.ls(l=True, sl=True, type=u"OpenVDBCopy")[0]

if prev_num == 0:
    pass
elif prev_num == 1:
    pm.connectAttr(prev_select[0] + ".VdbOutput", new + u".VdbInputA")
else:
    pm.connectAttr(prev_select[0] + ".VdbOutput", new + u".VdbInputA")
    pm.connectAttr(prev_select[1] + ".VdbOutput", new + u".VdbInputB")
