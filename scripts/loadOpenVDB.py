import pymel.core as pm

if pm.pluginInfo("OpenVDB", query=True, loaded=True):
    print "OpenVDB is already loaded!"
else:
    print "Loading OpenVDB!"
    pm.loadPlugin("OpenVDB")
    print "OpenVDB loaded!"
